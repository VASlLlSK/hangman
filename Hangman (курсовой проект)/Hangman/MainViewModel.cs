﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EasyHttp.Http;

namespace Hangman
{
    public class NPC : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    class MainViewModel : NPC
    {
        char[] currentWordChar;
        string currentWordString;
        public string CurrentWord { get { return currentWordString; } set { currentWordString = value; OnPropertyChanged(); } }
        string secretWord;
        public List<myChar> Alfabet { get; set; }
        public CurrentUser CUser { get; set; }
        public MyCategory myCategory { get; set; }
        public ObservableCollection<User> Rating { get; set; }
        public Stack<DockPanel> Page { get; set; }
        string tip;
        public string TIP { get { return tip; } set { tip = value; OnPropertyChanged(); } }
        int fiftyfifty = 0;
        int ors = 0;
        string currentTable;
        MainWindow ui;
        public MainViewModel(MainWindow mainWindow)
        {
            Alfabet = new List<myChar>();
            Rating = new ObservableCollection<User>();
            Page = new Stack<DockPanel>();
            CUser = new CurrentUser();
            myCategory = new MyCategory();

            currentTable = "";
            TIP = "";
            ui = mainWindow;

            for (int i = 'А', j = 0; i <= 'Я'; i++)
            {
                var temp = new myChar();
                temp.c = (char)i;
                temp.X = (i - 'А' + j) / 11;
                temp.Y = (i - 'А' + j) % 11;
                temp.visibility = Visibility.Visible;
                Alfabet.Add(temp);
                if (i == 1045)
                {
                    j = 1;
                    temp = new myChar();
                    temp.c = (char)1025;
                    temp.X = 0;
                    temp.Y = 6;
                    temp.visibility = Visibility.Visible;
                    Alfabet.Add(temp);
                }
            }

            secretWord = "";
            currentWordChar = new char[secretWord.Length * 2];
            CurrentWord = new string(currentWordChar);

        }

        public void openChar(char c)
        {
            for (int i = 0; i < secretWord.Length; i++)
            {
                if (secretWord[i] == c)
                {
                    currentWordChar[i * 2] = c;
                }
            }
            CurrentWord = new string(currentWordChar);
        }

        public void GetNewWord(string table)
        {
            var client = new HttpClient();
            string url = "https://localhost:44335/api/Words";

            if (table == "customCategory")
            {
                url += "?Alife=" + myCategory.Alife;
                url += "&Length=" + myCategory.Length;
                url += "&OpenSymbol=" + myCategory.OpenSymbol; 
                url += "&BeginVowel=" + myCategory.BeginVowel;
                url += "&BeginConsonant=" + myCategory.BeginConsonant;
                url += "&EndVowel=" + myCategory.EndVowel;
                url += "&EndConsonant=" + myCategory.EndConsonant;
                url += "&Man=" + myCategory.Man;
                url += "&Woman=" + myCategory.Woman;
                url += "&Neuter=" + myCategory.Neuter;
            }
            else
                url += "/" + table;

            var response = client.Get(url);
            var wordFromServer = response.StaticBody<string>();
            if (wordFromServer == null)
            {
                wordFromServer = "перейдите в след. категорию";
            }
            secretWord = wordFromServer.ToUpper();
            currentWordChar = new char[secretWord.Length * 2];
            for (int i = 0; i < secretWord.Length * 2; i++)
            {
                currentWordChar[i] = '_';
                i++;
                currentWordChar[i] = ' ';
            }
            CurrentWord = new string(currentWordChar);
        }
        public void Rules() 
        {
            if (secretWord.Equals(CurrentWord.ToString().Replace(" ", "")) || CUser.Life == 0)
            {
                if (CUser.Life == 0)
                {
                    CUser.score = 0;
                    ors = 0;
                    fiftyfifty = 0;
                    // lose animation
                    // lose sound
                }
                else
                {
                    CUser.score++;
                    if (ors !=0)
                        ors++;
                    if (fiftyfifty != 0)
                        fiftyfifty++;
                    // win animation
                    // win sound
                }

                OpenAllAlfabetSymbols();
                CUser.Life = 7;
                GetNewWord(currentTable);
            }
            
            
        }
        public void OpenAllAlfabetSymbols()
        {
            Alfabet.ForEach(delegate (myChar symbol)
            {
                symbol.visibility = Visibility.Visible;
            });
        }

        public void VisibleSomeDockPanel(DockPanel ui)
        {
            ui.Visibility = Visibility.Visible;
        }
        public void CollapsedSomeDockPanel(DockPanel ui)
        {
            ui.Visibility = Visibility.Collapsed;
        }
        public void FillTIPforCategory()
        {
            TIP = "Иникальные слова будут выдоваться до тех пор пока вы не проиграете. После проигрыша ваши достижения будут обновленны.\n";
            TIP += "В категории Хардкор собраны все слова из словаря, в именительном падеже.\n";
            TIP += "В категории \"Свой режим\" вы можете настроить под себя характиристики выдоваемых слов.\n\n";
            TIP += "ПРИМЕЧАНИЕ: В режиме \"Свой режим\" могут попадаться слова которые вы уже отгадывали. В этом режиме ваши достижения не будут сохранены.";
        }
        public void FillTIPforGame()
        {
            TIP = "МАЛЕНЬКИЕ ХИТРОСТИ:\n";
            TIP += "• Слово легче отгадывать, когда известны согласные, входящие в него.\n";
            TIP += "• Чем длиннее слово, тем проще его угадать.\n";
            TIP += "• Начинать отгадывать имеет смысл с наиболее чаще встречающихся букв: о, е, и, а,...\n";
            TIP += "ПОДСКАЗКИ:\n";
            TIP += "• 50/50 - убирает 50% букв. После использования подсказки она будет перезаряжаться 3 хода\n";
            TIP += "• ORS - открывает случайную букву в слове. После использования подсказки она будет перезаряжаться 2 хода\n";
        }
        private RelayCommand _previousPage;
        public RelayCommand PreviousPage
        {
            get
            {
                return _previousPage ??
                  (_previousPage = new RelayCommand(x =>
                  {
                      var someDockPanel = Page.Pop();
                      CollapsedSomeDockPanel(someDockPanel);
                      someDockPanel = Page.Peek();
                      VisibleSomeDockPanel(someDockPanel);

                      if (someDockPanel.Name.ToString() == "MENU")
                          ui.BAR.Visibility = Visibility.Collapsed;

                      if (someDockPanel.Name.ToString() == "CATEGORIES") 
                      {
                          ui.TIP.Visibility = Visibility.Visible;
                          ui.NEXT.Visibility = Visibility.Collapsed;
                          ui.GAMEBAR.Visibility = Visibility.Hidden;
                          FillTIPforCategory();
                      }

                      if (someDockPanel.Name.ToString() == "CUSTOM")
                      {
                          ui.GAMEBAR.Visibility = Visibility.Hidden;
                          ui.NEXT.Visibility = Visibility.Visible;
                          ui.TIP.Visibility = Visibility.Collapsed;
                      }

                      if (someDockPanel.Name.ToString() == "GAME")
                      {
                          ui.GAMEBAR.Visibility = Visibility.Visible;
                          ui.NEXT.Visibility = Visibility.Collapsed ;
                          ui.TIP.Visibility = Visibility.Visible;
                      }
                  }, x => {
                      if (Page.Count <= 1)
                        return false;
                      return true;
                  }));
            }
        }

        private RelayCommand _pageSwitcher;
        public RelayCommand PageSwitcher
        {
            get
            {
                return _pageSwitcher ??
                  (_pageSwitcher = new RelayCommand(x =>
                  {
                      var dock = x as DockPanel;

                      switch (dock.Name)
                      {
                          case "CATEGORIES":
                              {
                                  CollapsedSomeDockPanel(ui.MENU);
                                  VisibleSomeDockPanel(ui.BAR);
                                  VisibleSomeDockPanel(dock);
                                  Page.Push(dock);
                                  ui.TIP.Visibility = Visibility.Visible;
                                  FillTIPforCategory();
                                  break;
                              }
                          case "LK":
                              {
                                  //save stats
                                  //LeaveAccount

                                  var someDockPanel = Page.Peek();
                                  CollapsedSomeDockPanel(someDockPanel);
                                  VisibleSomeDockPanel(dock);
                                  Page.Push(dock);
                                  break;
                              }
                          case "SETTINGS":
                              {
                                  var someDockPanel = Page.Peek();
                                  CollapsedSomeDockPanel(someDockPanel);
                                  VisibleSomeDockPanel(dock);
                                  Page.Push(dock);
                                  break;
                              }
                              
                          default: break;
                      }

                  }));
            }
        }

        private RelayCommand _openGame;
        public RelayCommand OpenGame
        {
            get
            {
                return _openGame ??
                  (_openGame = new RelayCommand(x =>
                  {
                      var button = x as Button;

                      if (button.Name.ToString() == "customCategory")
                      {
                          CollapsedSomeDockPanel(ui.CATEGORIES);
                          ui.GAMEBAR.Visibility = Visibility.Hidden;
                          ui.NEXT.Visibility = Visibility.Visible;
                          ui.TIP.Visibility = Visibility.Collapsed;
                          VisibleSomeDockPanel(ui.CUSTOM);
                          Page.Push(ui.CUSTOM);
                      }
                      else
                      {
                          CollapsedSomeDockPanel(ui.CATEGORIES);
                          ui.GAMEBAR.Visibility = Visibility.Visible;
                          VisibleSomeDockPanel(ui.GAME);
                          Page.Push(ui.GAME);
                          GetNewWord(button.Name.ToString());
                          currentTable = button.Name.ToString();
                          FillTIPforGame();
                          OpenAllAlfabetSymbols();
                      }

                  }));
            }
        }
        private RelayCommand _getCustomWord;
        public RelayCommand GetCustomWord
        {
            get
            {
                return _getCustomWord ??
                  (_getCustomWord = new RelayCommand(x =>
                  {
                      var button = x as Button;

                      CollapsedSomeDockPanel(ui.CUSTOM);
                      ui.GAMEBAR.Visibility = Visibility.Visible;
                      ui.NEXT.Visibility = Visibility.Collapsed;
                      ui.TIP.Visibility = Visibility.Visible;
                      VisibleSomeDockPanel(ui.GAME);
                      Page.Push(ui.GAME);
                      GetNewWord(button.Name.ToString());
                      currentTable = button.Name.ToString();
                      OpenAllAlfabetSymbols();
                  }));
            }
        }
        

        private RelayCommand _openChar;
        public RelayCommand OpenChar
        {
            get
            {
                return _openChar ??
                  (_openChar = new RelayCommand(x =>
                  {
                      var ch = x as myChar;

                      if (secretWord.Contains(ch.c))
                      {
                          openChar(ch.c);
                      }
                      else
                      {
                          CUser.Life--;
                      }

                      ch.visibility = Visibility.Hidden;

                      Rules();
                      
                  }));
            }
        }

        private RelayCommand _fiftyFifty;
        public RelayCommand FiftyFifty
        {
            get
            {
                return _fiftyFifty ??
                  (_fiftyFifty = new RelayCommand(x =>
                  {
                      int countVisibleChar = 0;
                      foreach (var c in Alfabet)
                      {
                          if (c.visibility == Visibility.Visible)
                              countVisibleChar++;
                      }

                      var rnd = new Random();
                      for (int i=0; i < countVisibleChar / 2;)
                      {
                          var j = rnd.Next() % 33;
                          if (!secretWord.Contains(Alfabet[j].c) && Alfabet[j].visibility != Visibility.Hidden)
                          {
                              Alfabet[j].visibility = Visibility.Hidden;
                              i++;
                          }

                      }

                      fiftyfifty = -3;

                  }, x => 
                  {
                      if (fiftyfifty < 0)
                          return false;
                      int countVisibleChar = 0;
                      int minC = 0;
                      foreach (var c in Alfabet)
                      {
                          if (c.visibility == Visibility.Visible)
                              countVisibleChar++;
                          if (secretWord.Contains(c.c))
                              minC++;
                      }
                      if ((countVisibleChar / 2 ) <= minC)
                          return false;
                      if (secretWord.Equals(CurrentWord.ToString().Replace(" ", "")))
                          return false;

                      return true;
                  }));
            }
        }

        private RelayCommand _openRandomSymbol;
        public RelayCommand OpenRandomSymbol
        {
            get
            {
                return _openRandomSymbol ??
                  (_openRandomSymbol = new RelayCommand(x =>
                  {
                      var rnd = new Random();
                      for (; ;)
                      {
                          var j = rnd.Next() % secretWord.Length;
                          if (currentWordChar[j * 2] == '_')
                          {
                              openChar(secretWord[j]);
                              var someChar = Alfabet.Find(X => X.c == secretWord[j]);
                              someChar.visibility = Visibility.Hidden;
                              break;
                          }

                      }

                      ors = -3;
                      Rules();

                  },x => {
                      if (ors < 0)
                          return false;
                      if (secretWord.Equals(CurrentWord.ToString().Replace(" ", "")))
                          return false;
                      return true;
                  }));
            }
        }


        private RelayCommand _openRating;
        public RelayCommand OpenRating
        {
            get
            {
                return _openRating ??
                  (_openRating = new RelayCommand(x =>
                  {
                      var ch = x as myChar;
                      var client = new HttpClient();
                      string url = "https://localhost:44335/api/RatingTop10";
                      var response = client.Get(url);
                      var rating = response.StaticBody<User[]>();
                      int i = 1;
                      Rating.Clear();
                      foreach (var u in rating)
                      {
                          u.place = i;
                          Rating.Add(u);
                          i++;
                      }
                      var someDockPanel = Page.Peek();

                      if (someDockPanel != ui.RATING)
                      {
                          ui.BAR.Visibility = Visibility.Visible;
                          ui.TIP.Visibility = Visibility.Hidden;
                          CollapsedSomeDockPanel(someDockPanel);
                          VisibleSomeDockPanel(ui.RATING);
                          Page.Push(ui.RATING);
                      }
                          
                  }));
            }
        }


        private RelayCommand login;
        public RelayCommand LOGIN  /////////////////////////////////////////////////////////////////////////
        {
            get
            {
                return login ??
                  (login = new RelayCommand(x =>
                  {
                      //save stats
                      ui.Close();
                  }));
            }
        }


        private RelayCommand regisration;
        public RelayCommand Regisration  /////////////////////////////////////////////////////////////////////////
        {
            get
            {
                return regisration ??
                  (regisration = new RelayCommand(x =>
                  {
                      //save stats
                      ui.Close();
                  }));
            }
        }

        private RelayCommand exit;
        public RelayCommand EXIT  /////////////////////////////////////////////////////////////////////////
        {
            get
            {
                return exit ??
                  (exit = new RelayCommand(x =>
                  {
                      //save stats
                      ui.Close();
                  }));
            }
        }


    }

    public class RelayCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }

    public class myChar : NPC
    {
        public int X { get; set; }
        public int Y { get; set; }
        public char c { get; set; }
        Visibility _visibility;
        public Visibility visibility { get { return _visibility; } set { _visibility = value; OnPropertyChanged(); } }
    }

    public class User : NPC
    {
        int _score;
        public int score { get { return _score; } set { _score = value; OnPropertyChanged(); } }
        public int place { get; set; }
        public string name { get; set; }
    }
    public class CurrentUser : User
    {
        int _life;
        public int Life { get { return _life; } set { _life = value; OnPropertyChanged(); } }
        string _password;
        public string password { get { return _password; } set { _password = value; OnPropertyChanged(); } }

        public CurrentUser()
        {
            score = 0;
            Life = 7;
            name = "";
            password = "";
        }

    }
    public class MyCategory : NPC
    {
        bool _alife;
        int  _length = 4;
        int  _openSymbol;
        bool _beginVowel;
        bool _beginConsonant;
        bool _endVowel;
        bool _endConsonant;
        bool _man;
        bool _woman;
        bool _neuter;

        public bool Alife { get { return _alife; } set { _alife = value; OnPropertyChanged(); } }
        public int Length { get { return _length; } set { _length = value; OnPropertyChanged(); } }
        public int OpenSymbol { get { return _openSymbol; } set { _openSymbol = value; OnPropertyChanged(); } }
        public bool BeginVowel { get { return _beginVowel; } set { _beginVowel = value; OnPropertyChanged(); } }
        public bool BeginConsonant { get { return _beginConsonant; } set { _beginConsonant = value; OnPropertyChanged(); } }
        public bool EndVowel { get { return _endVowel; } set { _endVowel = value; OnPropertyChanged(); } }
        public bool EndConsonant { get { return _endConsonant; } set { _endConsonant = value; OnPropertyChanged(); } }
        public bool Man { get { return _man; } set { _man = value; OnPropertyChanged(); } }
        public bool Woman { get { return _woman; } set { _woman = value; OnPropertyChanged(); } }
        public bool Neuter { get { return _neuter; } set { _neuter = value; OnPropertyChanged(); } }
    }
}