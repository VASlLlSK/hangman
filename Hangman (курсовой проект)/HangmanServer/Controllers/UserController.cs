﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using HangmanServer.Models;

namespace HangmanServer.Controllers
{
    public class UserController : ApiController
    {
        private DataBase db;
        public UserController()
        {
            db = new DataBase();
        }

        // GET api/User
        [Route("api/User")]
        public async Task<string> Get([FromUri] SettingsForSearch settings)
        {
            return await Task.Run(() => {
                return db.GetSpesialWord(settings);
            });
        }

        // GET api/RaitingTop10
        [Route("api/RatingTop10")]
        public async Task<List<User>> Get()
        {
            return await Task.Run(() => {
                return db.GetRatingTop10();
            });
        }

        // POST api/Words
        public void Post([FromBody]string value)
        {
        }

        // PUT api/Words/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/Words/5
        public void Delete(int id)
        {
        }
    }
}
