﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using HangmanServer.Models;

namespace HangmanServer.Controllers
{
    public class WordsController : ApiController
    {
        private DataBase db;
        public WordsController() {
            db = new DataBase();
        }

        // GET api/Words
        [Route("api/Words")]
        public async Task<string> Get([FromUri] SettingsForSearch settings)
        {
            return await Task.Run(() => {
                return db.GetSpesialWord(settings);
            });
        }

        // GET api/Words/table
        [Route("api/Words/{table}")]
        public async Task<string> Get(string table)
        {
            int id=0;
            return await Task.Run(() => {
                return db.GetWordFromSomeTable(table, id);
            });
        }

        // POST api/Words
        public void Post([FromBody]string value)
        {
        }

        // PUT api/Words/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/Words/5
        public void Delete(int id)
        {
        }
    }
}
