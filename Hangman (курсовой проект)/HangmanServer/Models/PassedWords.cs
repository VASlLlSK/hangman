﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace HangmanServer.Models
{
    public class PassedWords
    {
        public List<int> animals { get; set; }
        public List<int> cities { get; set; }
        public List<int> country { get; set; }
        public List<int> hobby { get; set; }
        public List<int> nouns { get; set; }
        public List<int> sport { get; set; }

        public List<int> getArrayFromTable(string table)
        {
            switch (table)
            {
                case "animals":
                    return animals;
                case "cities":
                    return cities;
                case "country":
                    return country;
                case "hobby":
                    return hobby;
                case "nouns":
                    return nouns;
                case "sport":
                    return sport;
                default: return null;
            }
        }
    }
}