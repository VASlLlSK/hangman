﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using Dapper;
using System.Dynamic;
using Newtonsoft.Json;

namespace HangmanServer.Models
{
    public class DataBase
    {
        string _connectionString = "Server=localhost;Port=3306;Database=todolist;Uid=root;Pwd=54321ASASAS;";
        private MySqlConnection connection;
        public DataBase()
        {
            connection = new MySqlConnection(_connectionString);
        }

        public MySqlConnection GetConnection()
        {
            return connection;
        }

        private void OpenConnection()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
                connection.Open();
        }

        private void CloseConnection()
        {
            if (connection.State == System.Data.ConnectionState.Open)
                connection.Close();
        }

        public string GetWordFromSomeTable(string table, int id)
        {
            OpenConnection();
            Word word = null;

            var sql = "SELECT id, word FROM hangman." + table + " as t,";
            sql += "(SELECT ROUND((SELECT MAX(id) FROM hangman." + table + ") * rand()) as rnd ";
            sql += "FROM hangman." + table + " LIMIT 1) tmp ";
            sql += "WHERE id in (rnd) ";
            sql += "ORDER BY word";

            word = connection.Query<Word>(sql).First();


            sql = "SELECT PassedWords FROM hangman.users WHERE id = @id";
            var passedWordsJSON = connection.Query<string>(sql, new { id = 1}).First();

            PassedWords passedWords = JsonConvert.DeserializeObject<PassedWords>(passedWordsJSON);

            var passedIDFromSomeTable = passedWords.getArrayFromTable(table);

            if (!passedIDFromSomeTable.Contains(word.id))
            {
                passedIDFromSomeTable.Add(word.id);
            }
            else
            {
                var str = ""; 
                foreach (var strID in passedIDFromSomeTable)
                {
                    str += strID.ToString()+ ", ";
                }
                str = str.Remove(str.Length - 2, 2);
                sql = "SELECT id,word FROM hangman." + table + " WHERE id NOT IN ("+str+")";
                word = connection.Query<Word>(sql).FirstOrDefault();
                if (word == null || word.word == "")
                    return null;
                passedIDFromSomeTable.Add(word.id);
            }

            string json = JsonConvert.SerializeObject(passedWords);

            sql = "UPDATE hangman.users SET PassedWords = @PassedWords WHERE id = @id";

            connection.Query<string>(sql, new { id = 1, PassedWords = json});

            CloseConnection();
            return word.word;
        }
        

        public string GetSpesialWord(SettingsForSearch settings)
        {
            OpenConnection();

            var sql = "SELECT Count(*) FROM hangman.nouns WHERE wcase = \"им\"";
            sql += settings.GetAlifePartQuery(true);
            sql += settings.GetBeginEndPartQuery(true);
            sql += settings.GetGenderPartQuery(true);
            sql += settings.GetLengthPartQuery(true);

            var count = connection.Query<int>(sql).First();

            sql = "SELECT word FROM ( SELECT word FROM hangman.nouns WHERE wcase = \"им\"";
            sql += settings.GetAlifePartQuery(true);
            sql += settings.GetBeginEndPartQuery(true);
            sql += settings.GetGenderPartQuery(true);
            sql += settings.GetLengthPartQuery(true);
            sql += ") as t ORDER BY rand() LIMIT 1";

            var word = connection.Query<string>(sql).FirstOrDefault();

            CloseConnection();
            return word;
        }

        public List<User> GetRatingTop10()
        {
            var sql = "SELECT Name,Score FROM hangman.users ORDER BY Score DESC, Score DESC LIMIT 10";
            var result = connection.Query<User>(sql).ToList();
            return result;
        }

        //public List<Task> GetAllTasks()
        //{
        //    List<Task> tasks = new List<Task>();

        //    var sql = "SELECT * FROM todolist.tasks";

        //    tasks = connection.Query<Task>(sql).ToList();
        //    return tasks;
        //}

        //public void CreateTask(Task task)
        //{
        //    var sql = "INSERT INTO todolist.tasks (id, Title, TaskContent, Done) VALUES (@id, @Title, @TaskContent, @Done)";

        //    connection.Query<Task>(sql, new { id = task.id, Title = task.Title, TaskContent = task.TaskContent, Done = task.Done });
        //}

        //public void EditTask(Task task)
        //{
        //    var sql = "UPDATE todolist.tasks SET Title = @Title, TaskContent = @TaskContent WHERE id = @id";
        //    connection.Query<Task>(sql, new { id = task.id, Title = task.Title, TaskContent = task.TaskContent });
        //}

        //public void UpdateDoneTask(Task task)
        //{
        //    var sql = "UPDATE todolist.tasks SET Done = @Done WHERE id = @id";
        //    connection.Query<Task>(sql, new { id = task.id, Done = task.Done });
        //}

        //public void DeleteTask(int id)
        //{
        //    var sql = "DELETE FROM todolist.tasks WHERE id = @id";
        //    connection.Query<Task>(sql, new { id });
        //}

        //public Task FindTask(int id)
        //{
        //    var sql = "SELECT * FROM todolist.tasks WHERE id=@id";

        //    var task = connection.Query<Task>(sql, new { id }).First();
        //    return task;
        //}

        //public Task GetLastTask()
        //{
        //    var sql = "SELECT* FROM todolist.tasks WHERE id = LAST_INSERT_ID();";
        //    var task = connection.Query<Task>(sql).First();
        //    return task;
        //}
    }

}