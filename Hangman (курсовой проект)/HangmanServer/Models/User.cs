﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangmanServer.Models
{
    public class User
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Password  { get; set; }
        public string PassedWords { get; set; }
        public int Score { get; set; }
    }
}