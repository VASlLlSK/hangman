﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySqlX.XDevAPI.Common;

namespace HangmanServer.Models
{
    public class SettingsForSearch
    {
        public bool Alife { get; set; }
        public int  Length { get; set; }
        public int  OpenSymbol { get; set; }
        public bool BeginVowel { get; set; }
        public bool BeginConsonant { get; set; }
        public bool EndVowel { get; set; }
        public bool EndConsonant { get; set; }
        public bool Man { get; set; }
        public bool Woman { get; set; }
        public bool Neuter { get; set; }

        public string GetGenderPartQuery(bool addAND)
        {
            bool more = false;
            string result = "";

            if (!(Man || Woman || Neuter))
                return result;

            if (addAND)
                result = " AND";
            result += " (";

            if (Man == true)
            {
                result += "gender = \"муж\"";
                more = true;
            }

            if (Woman == true)
            {
                if (more)
                {
                    result += " OR";
                }
                result += " gender = \"жен\"";
                more = true;
            }

            if (Neuter == true)
            {
                if (more)
                {
                    result += " OR";
                }
                result += " gender = \"ср\"";
            }

            result += ")";
            return result;
        }

        public string GetBeginEndPartQuery(bool addAND)
        {
            string result = "";

            if ((BeginVowel && BeginConsonant && EndVowel && EndConsonant) ||
                !(BeginVowel || BeginConsonant || EndVowel || EndConsonant))
                return result;

            if (addAND)
                result = " AND";
            result += " word RLIKE '";


            if (BeginVowel ^ BeginConsonant)
            {
                if (BeginVowel)
                {
                    result += "^[ауоыиэяюёе]";
                }
                else
                {
                    result += "^[бвгджзйклмнпрстфхцчшщ]";
                }
            }

            if (EndVowel ^ EndConsonant)
            {
                if (EndVowel)
                {
                    result += ".*[ауоыиэяюёе]$";
                }
                else
                {
                    result += ".*[бвгджзйклмнпрстфхцчшщьъ]$";
                }
            }

            result += "'";

            return result;
        }

        public string GetAlifePartQuery(bool addAND)
        {
            string result = "";

            if (addAND)
                result = " AND";

            if (Alife)
                result += " soul = 1";
            else
                result += " soul = 0";

            return result;
        }

        public string GetLengthPartQuery(bool addAND)
        {
            string result = "";

            if (addAND)
                result = " AND";


            result += " CHAR_LENGTH(word) = " + Length;      

            return result;
        }

    }
}